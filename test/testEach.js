const Each = require('../Each')

var arr=[2,4,6,8]

Each(arr,cb=(ele,idx,ar)=>{
    console.log(" element = %d :index = %s :array= %s",ele,idx,ar) 
    

    /* output:

 element = 2 :index = 0 :array= [ 2, 4, 6, 8 ]
 element = 4 :index = 1 :array= [ 2, 4, 6, 8 ]
 element = 6 :index = 2 :array= [ 2, 4, 6, 8 ]
 element = 8 :index = 3 :array= [ 2, 4, 6, 8 ]




    */

})