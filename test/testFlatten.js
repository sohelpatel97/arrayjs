const flatten = require('../flatten')

let arr = [1, [2], [[3]], [[[4]]]]

const result = flatten(arr)  //pass array
 
console.log(result) // [ 1, 2, 3, 4 ]