
const MapF = function(array,callback) {
    let newArray=[] 

    for (let i = 0; i < array.length; i++){
        newArray.push(callback(array[i],i,array)); 
    }
    return newArray   //return [ 2, 4, 6, 8, 10, 10 ]
};


module.exports  = MapF


