

const Find=(array,callback)=>{

    for ( let i = 0;i<array.length;i++) {

      if(callback( array[i],i,array)){   // if true

          return array[i] //return element which satisfy condition first from array 
      }
      else{
          
          continue;
      }
      
      return  //reurn undefined
    }
    
  }
  
module.exports = Find;