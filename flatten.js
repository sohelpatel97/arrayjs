


function flatten(arr){
    
    let newArr = [];
    
    function arrayChecker(ar){
    
        for(let i = 0; i < ar.length; i++){
            
            if(ar[i].constructor === Array){  // if item type is Array
                
                arrayChecker(ar[i])   // Recurssive calling
        }
        
            else{
                
                newArr.push(ar[i])   // Push if item is NOT Array
            }
          
        }
    }  
    
    arrayChecker(arr)  // 1'st call of arrayChecker
    
    return newArr
};


module.exports = flatten;