
// passing array,callback and initial value ()

function Reduce(array,callback,initialValue){

    let collector 

    if(initialValue != undefined){

        collector = initialValue; // assign collector initial value if given 
    }
  
    for(let i=0;i<array.length;i++){

        if(collector == undefined){

            collector = array[i];   // assign collector First element  if Not given 
        }

        else{

            collector = callback(collector,array[i]) // cb()  
        }
    }

    return collector

};


module.exports = Reduce;